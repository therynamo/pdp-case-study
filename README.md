Target PDP Case Study
===

### Scenario

myRetail is a rapidly growing company with HQ in Richmond, VA and over 200 stores across the east coast. The company's annual revenue last year was $5 billion and growing at the rate of 10% year over year. myRetail sells general merchandise products, including a fast growing fresh grocery segment. The stores average 80,000 sq. ft. in size and carry around 20,000 products. myRetail wants to provide a multi-channel experience for its customers online.

myRetail is comparing solution options for the online grocery store. Create a web application that does the following:

1) Use the provided design docs, desktop.psd and mobile.psd, as general guidelines to create the user interface.

2) Use the provided json file, `item-data.json`, to populate the title, images and price.

3) The client side code can be backbone, angular or another framework of your choice. React (additional library's such as Redux are welcome). Use node to run the application.

4) Show the add to cart button only if the item is available online, purchasingChannelCode equals 0 or 1.

5) Show the pick up in store only if the item is available instore, purchasingChannelCode equals 0 or 2.

6) Create a carousel to scroll through the images

### Deliverables

1.Test your code -simulate functional testing needs by generating test scripts (automation test cases preferred).

2.Build your code and package using gulp, grunt or other tools of your choice.

3.Provide approach to deploy code in live environment -continuous delivery flow diagram will suffice.

4.Store code in a public git repository like GitHub, GitLab, BitBucket, or Visual Studio Team Services and forward link to Staffing Specialist and Recruiter

Getting The Project Running
===

```bash
# install dependencies
yarn
# run tests
yarn test

# run project
yarn start
```

How This Could Be Deployed
===

Refer to the `ContinuousIntegrationContinuousDelivery.png` at the root of the project if this image doesn't load.

![ ](ContinuousIntegrationContinuousDelivery.png)
