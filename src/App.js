import React, { Component } from 'react';
import { css } from 'emotion';

import productData from './data/item-data.json';

import ProductContainer from './containers/ProductContainer';
import ReviewsContainer from './containers/ReviewsContainer';

const appStyles = css`
  display: flex;
  flex-direction: column;
  max-width: 375px;

  @media (min-width: 420px) {
    max-width: 100%;
    margin-top: 100px;
  }
`;

const productContainerStyles = css`
  width: 100%;

  @media (min-width: 420px) {
    display: flex;
    justify-content: center;
  }
`;

const reviewContainerStyles = css`
  margin-left: 5px;
  margin-bottom: 15px;

  @media (min-width: 420px) {
    display: flex;
    justify-content: center;
  }
`;

const spacer = css`
  width: 445px;
`;

class App extends Component {
  render() {
    return (
      <div className={appStyles}>
        <div className={productContainerStyles}>
          <ProductContainer data={productData.CatalogEntryView[0]} />
        </div>
        <div className={reviewContainerStyles}>
          <ReviewsContainer data={productData.CatalogEntryView[0]} />
          <div className={spacer}/>
        </div>
      </div>
    );
  }
}

export default App;
