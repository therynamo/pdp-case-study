import React from 'react';
import PropTypes from 'prop-types';

import styles from './styles';

const Price = ({ price, qualifier }) => (
  <div className={styles.wrapper}>
    <span className={styles.price}>{price}</span>
    <span className={styles.qualifier}>{qualifier.toLowerCase()}</span>
  </div>
);

Price.propTypes = {
  price: PropTypes.string,
  qualifier: PropTypes.string,
};

Price.defaultProps = {
  price: '$0.00',
  qualifier: 'Online Price',
};

export default Price;
