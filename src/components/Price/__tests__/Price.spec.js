import React from 'react';
import renderer from 'react-test-renderer'

import Price from '../Price';

describe('Price', () => {
  it('should render without props', () => {
    const wrap = renderer.create(<Price />);

    expect(wrap).toMatchSnapshot();
  });
  it('should render given props', () => {
    const wrap = renderer.create(
      <Price
        price="$700.00"
        qualifier="In Store Only"
      />
    );

    expect(wrap).toMatchSnapshot();
  });
});
