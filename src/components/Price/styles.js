import { css } from 'emotion';

const styles = {
  price: css`
    font-size: 23px;
    font-weight: bold;
    padding-right: 5px;
  `,
  qualifier: css`
    font-size: 9px;
  `,
  wrapper: css`
    margin: 0 0 15px 5px;
  `,
};

export default styles;
