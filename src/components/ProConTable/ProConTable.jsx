import React from 'react';
import PropTypes from 'prop-types';

import ReviewContent from './ReviewContent';

import styles from './styles';

const dateRegex = /\W(.+)/ig;

const formatDate = (date) => {
  const dateString = date.toDateString();
  const formattedDate = dateString.match(dateRegex)[0].trim();

  return formattedDate;
}

const ProConTable = ({ pro, con }) => {
  const proDate = new Date(pro.datePosted);
  const conDate = new Date(con.datePosted);

  return (
    <div className={styles.wrapper}>
      <div className={styles.heading}>
        <div>
          <span className={styles.bold}>PRO</span>
          <span className={styles.support}>most helpful 4-5 star review</span>
        </div>
        <div>
          <span className={styles.bold}>CON</span>
          <span className={styles.support}>most helpful 1-2 star review</span>
        </div>
      </div>
      <hr className={styles.hr}/>
      <div className={styles.content}>
        <ReviewContent
          title={pro.title}
          review={pro.review}
          screenName={pro.screenName}
          date={formatDate(proDate)}
          rating={pro.overallRating}
        />
        <ReviewContent
          title={con.title}
          review={con.review}
          screenName={con.screenName}
          date={formatDate(conDate)}
          rating={con.overallRating}
        />
      </div>
    </div>
  )
};

ProConTable.propTypes = {
  pro: PropTypes.shape({
    overallRating: PropTypes.string,
    review: PropTypes.string,
    screenName: PropTypes.string,
    title: PropTypes.string,
    datePosted: PropTypes.string,
  }),
  con: PropTypes.shape({
    overallRating: PropTypes.string,
    review: PropTypes.string,
    screenName: PropTypes.string,
    title: PropTypes.string,
    datePosted: PropTypes.string,
  }),
};

ProConTable.defaultProps = {
  pro: {},
  con: {},
};

export default ProConTable;
