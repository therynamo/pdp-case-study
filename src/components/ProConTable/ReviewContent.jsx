import React from 'react';
import PropTypes from 'prop-types';
import { css } from 'emotion';

import Stars from '../Stars';

import styles from './styles.reviewContent';

const ReviewContent = ({ title, review, screenName, date, rating }) => (
  <div className={styles.content}>
    <Stars rating={rating} />
    <span className={styles.bold}>{title}</span>
    <p className={styles.small}>{review}</p>
    <div>
      <span className={css`${styles.small} ${styles.meta}`}>{screenName}</span>
      <span className={styles.small}>{date}</span>
    </div>
  </div>
);

ReviewContent.propTypes = {
  title: PropTypes.string.isRequired,
  review: PropTypes.string.isRequired,
  screenName: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired,
  rating: PropTypes.string.isRequired,
};

export default ReviewContent;
