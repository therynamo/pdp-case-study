import React from 'react';
import renderer from 'react-test-renderer';

import ProConTable from '../ProConTable';
import data from '../../../data/item-data.json';

describe('ProConTable', () => {
  const reviewData = data.CatalogEntryView[0]
  const { CustomerReview = [] } = reviewData;
  const reviews = CustomerReview[0] || {};
  const pro = reviews.Pro[0] || {};
  const con = reviews.Con[0] || {};

  it('should render', () => {
    const wrap = renderer.create(<ProConTable pro={pro} con={con} />);

    expect(wrap).toMatchSnapshot();
  });
});
