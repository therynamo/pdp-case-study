import React from 'react';
import renderer from 'react-test-renderer';

import ReviewContent from '../ReviewContent';

describe('ReviewContent', () => {
  it('should render', () => {
    const wrap = renderer.create(
      <ReviewContent
        title="Loved This Product"
        review="Had a great time using it!!!"
        screenName="Frank Sinatra"
        date="Mon Mar 11 13:13:55 UTC 2013"
        rating="4"
      />
    );

    expect(wrap).toMatchSnapshot();
  });
});
