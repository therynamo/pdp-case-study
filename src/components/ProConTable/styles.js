import { css } from 'emotion';

const styles = {
  bold: css`
    font-weight: bold;
  `,
  content: css`
    display: flex;
  `,
  heading: css`
    display: flex;
    width: 100%;

    & > div {
      display: flex;
      flex-direction: column;
    }

    & > div:first-child {
      margin-right: 55px;
    }
  `,
  hr: css`
    width: 100%;
    color: #CCC;
  `,
  support: css`
    font-size: 9px;
    color: #666;
  `,

  wrapper: css`
    display: flex;
    flex-direction: column;
    width: 345px;
    max-width: 350px;
    background-color: #F6F5F5;
    padding: 0 10px;
  `,
};

export default styles;
