import { css } from 'emotion';

import baseStyles from './styles';

const styles = {
  bold: baseStyles.bold,
  content: css`
    width: 150px;
    display: flex;
    flex-direction: column;
    flex: 1 0 150px;
    margin-left: 15px;
    padding-bottom: 10px;
  `,
  meta: css`
    padding-right: 5px;
    color: #0066CC;
  `,
  small: css`
    font-size: 9px;

    @media(min-width: 420px) {
      font-size: 12px;
    }
  `,
};

export default styles;
