import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { css } from 'emotion';

import styles from './styles.carouselItem';

class CarouselItem extends Component {
  handleOnClick = () => {
    const { onClick, src, order } = this.props;
    onClick(src, order)
  }

  handleOnKeyPress = (e) => {
    const { onClick, src, order } = this.props;
    const code = e.keyCode || e.which;

    if (code !== 13) return;

    onClick(src, order)
  }
  render () {
    const { src, order, isSelected, setImageRef } = this.props

    return (
      <div
        tabIndex="0"
        role="button"
        onClick={this.handleOnClick}
        onKeyPress={this.handleOnKeyPress}
        className={css`${styles.wrapper} ${isSelected ? styles.selected : ''}`}
        ref={(ref) => setImageRef(ref, order)}
      >
        <img
          className={styles.img}
          alt={`alternative view ${order + 1}`}
          src={src}
        />
      </div>
    );
  };
};

CarouselItem.propTypes = {
  src: PropTypes.string,
  onClick: PropTypes.func,
  order: PropTypes.number,
  isSelected: PropTypes.bool,
  setImageRef: PropTypes.func,
};

CarouselItem.defaultProps = {
  src: '',
  onClick: () => {},
  order: null,
  isSelected: false,
  setImageRef: () => {},
};

export default CarouselItem;
