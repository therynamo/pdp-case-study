import React from 'react';
import PropTypes from 'prop-types';

import CarouselItem from './CarouselItem';

import styles from './styles.imageSelectionCarousel';

const ImageSelectionCarousel = ({ images, onClick, selectedKey, setImageRef }) => (
  <div className={styles.wrapper}>
    {
      images.map(({ image = "" }, i) => (
        <CarouselItem
          src={image}
          onClick={onClick}
          key={i}
          order={i}
          isSelected={i === selectedKey}
          setImageRef={setImageRef}
        />
      ))
    }

  </div>
);

ImageSelectionCarousel.propTypes = {
  images: PropTypes.arrayOf(PropTypes.object),
  onClick: PropTypes.func,
  selectedKey: PropTypes.number,
  setImageRef: PropTypes.func,
};

ImageSelectionCarousel.defaultProps = {
  images: [],
  onClick: () => {},
  selectedKey: null,
  setImageRef: () => {},
};

export default ImageSelectionCarousel;
