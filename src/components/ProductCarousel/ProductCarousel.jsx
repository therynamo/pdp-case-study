import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { css } from 'emotion';

import ImageSelectionCarousel from './ImageSelectionCarousel';
import ViewLarger from './ViewLarger';

import styles from './styles';

class ProductCarousel extends Component {
  constructor(props) {
    super(props);

    const { Images } = this.props;
    const images = Images[0];
    const defaultImage = "https://upload.wikimedia.org/wikipedia/commons/a/ac/No_image_available.svg"

    this.imageRefs = {};

    this.setImageRef = (element, index) => {
      this.imageRefs[index] = element;
    }

    this.state = {
      displayImage: images.PrimaryImage[0].image || defaultImage,
      selectedKey: null,
    }
  }

  setProductImage = (imageUrl, key) => {
    this.setState(() => ({
      displayImage: imageUrl,
      selectedKey: key,
    }));
  }

  handleBack = (e) => {
    const code = e.keyCode || e.which;
    if (e.type !== 'click' && code !== 13) return;

    const minLength = 0;
    const maxLength = Object.keys(this.imageRefs).length - 1;
    let selectedKey = this.state.selectedKey;

    if(selectedKey <= minLength) {
      selectedKey = maxLength + 1;
    };

    this.setState(() => ({
      selectedKey: selectedKey - 1
    }), () => {
      this.imageRefs[this.state.selectedKey].focus();
      this.imageRefs[this.state.selectedKey].click();
    });
  };

  handleForward = (e) => {
    const code = e.keyCode || e.which;
    if (e.type !== 'click' && code !== 13) return;

    const maxLength = Object.keys(this.imageRefs).length - 1;
    let selectedKey = this.state.selectedKey;

    if(selectedKey >= maxLength) {
      selectedKey = -1;
    };

    this.setState(() => ({
      selectedKey: selectedKey + 1
    }), () => {
      this.imageRefs[this.state.selectedKey].focus();
      this.imageRefs[this.state.selectedKey].click();
    });
  };

  render() {
    const { Images } = this.props;
    const images = Images[0] || {};
    const alternateImages = images.AlternateImages || [];

    return (
      <div className={styles.wrapper}>
        <img
          className={styles.productImage}
          alt="product"
          src={this.state.displayImage}
        />
        <ViewLarger image={this.state.displayImage}/>
        <div className={styles.carouselWrapper}>
          <div
            onClick={this.handleBack}
            onKeyPress={this.handleBack}
            tabIndex="0"
            role="button"
            className={css`${styles.chevron} ${styles.back}`}
          />
          <ImageSelectionCarousel
            onClick={this.setProductImage}
            images={alternateImages}
            selectedKey={this.state.selectedKey}
            setImageRef={this.setImageRef}
          />
          <div
            onClick={this.handleForward}
            onKeyPress={this.handleForward}
            tabIndex="0"
            role="button"
            className={css`${styles.chevron} ${styles.forward}`}
          />
        </div>
      </div>
    )
  }
};

ProductCarousel.propTypes = {
  Images: PropTypes.arrayOf(PropTypes.object)
};

ProductCarousel.defaultProps = {
  Images: [{}]
};

export default ProductCarousel;
