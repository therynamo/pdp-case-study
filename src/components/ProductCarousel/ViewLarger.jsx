import React from 'react';
import PropTypes from 'prop-types';
import { css } from 'emotion';

import styles from './styles.viewLarger';

const ViewLarger = ({ onClick, image }) => (
  <div
    tabIndex="0"
    role="button"
    onClick={() => onClick(image)}
    className={styles.wrapper}
  >
    <div className={css`${styles.magnify}`}><i></i></div>
    <p>view larger</p>
  </div>
);

ViewLarger.propTypes = {
  image: PropTypes.string,
  onClick: PropTypes.func,
};

ViewLarger.defaultProps = {
  image: "",
  onClick: () => {},
};

export default ViewLarger;
