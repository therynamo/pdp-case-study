import React from 'react';
import renderer from 'react-test-renderer';
import { shallow } from 'enzyme';

import CarouselItem from '../CarouselItem';

describe('CarouselItem', () => {
  it('should render', () => {
    const wrap = renderer.create(<CarouselItem />);
    expect(wrap).toMatchSnapshot();
  });

  it('should handle onClick', () => {
    const clickStub = jest.fn()
    const wrap = shallow(
      <CarouselItem
        onClick={clickStub}
      />
    );

    const item = wrap.find('div');
    item.simulate('click');

    expect(clickStub).toHaveBeenCalled();
  });

  it('should call onClick on enter press', () => {
    const clickStub = jest.fn()
    const wrap = shallow(
      <CarouselItem
        onClick={clickStub}
      />
    );

    const item = wrap.find('div');
    item.simulate('keypress', { key: 'Enter', keyCode: 13 });

    expect(clickStub).toHaveBeenCalled();
  });

  it('should not call onClick on other key press', () => {
    const clickStub = jest.fn()
    const wrap = shallow(
      <CarouselItem
        onClick={clickStub}
      />
    );

    const item = wrap.find('div');
    item.simulate('keypress', { key: 'Escape' });

    expect(clickStub).not.toHaveBeenCalled();
  });
});
