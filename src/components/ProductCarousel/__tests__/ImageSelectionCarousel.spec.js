import React from 'react';
import renderer from 'react-test-renderer';
import { shallow } from 'enzyme';

import ImageSelectionCarousel from '../ImageSelectionCarousel';
import CarouselItem from '../CarouselItem';

describe('ImageSelectionCarousel', () => {
  it('should render', () => {
    const wrap = renderer.create(<ImageSelectionCarousel />);

    expect(wrap).toMatchSnapshot();
  });
  it('should render a list of CarouselItems', () => {
    const imageMock = [{ image: 'https://www.google.com' }, { image: 'https://www.target.com' }];
    const wrap = shallow(
      <ImageSelectionCarousel
        images={imageMock}
      />
    );

    const items = wrap.find(CarouselItem);

    expect(items.length).toEqual(2);
  });
});
