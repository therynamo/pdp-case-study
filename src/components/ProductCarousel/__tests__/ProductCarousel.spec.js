import React from 'react';
import renderer from 'react-test-renderer';
import { shallow, mount } from 'enzyme';

import ProductCarousel from '../ProductCarousel';
import CarouselItem from '../CarouselItem';

import data from '../../../data/item-data.json';

describe('ProductCarousel', () => {
  const { Images } = data.CatalogEntryView[0]

  it('should render', () => {
    const wrap = renderer.create(<ProductCarousel Images={Images} />);

    expect(wrap).toMatchSnapshot();
  });

  it('should replace product image when alt image is selected', () => {
    const wrap = mount(<ProductCarousel Images={Images} />);
    const expectedImage = "http://target.scene7.com/is/image/Target/14263758_Alt01";

    const firstItem = wrap.find(CarouselItem).at(0);
    firstItem.simulate('click');

    const image = wrap.find('img').at(0);

    expect(image.props().src).toEqual(expectedImage);
  });

  it('should select child ref on click of left chevron', () => {
    const wrap = mount(<ProductCarousel Images={Images} />);

    const leftChevron = wrap.find('div').at(4);
    leftChevron.simulate('click');

    const items = wrap.find(CarouselItem);
    const lastItem = items.at(items.length - 1);

    expect(lastItem.props().isSelected).toEqual(true);
  });

  it('should select child ref on click of right chevron', () => {
    const wrap = mount(<ProductCarousel Images={Images} />);

    const divs = wrap.find('div');
    const rightChevron = divs.at(divs.length - 1);
    rightChevron.simulate('click');

    const items = wrap.find(CarouselItem);
    const lastItem = items.at(1);

    expect(lastItem.props().isSelected).toEqual(true);
  });

  it('should select child ref on keypress of left chevron', () => {
    const wrap = mount(<ProductCarousel Images={Images} />);

    const leftChevron = wrap.find('div').at(4);
    leftChevron.simulate('keypress', { key: 'Enter', keyCode: 13 });

    const items = wrap.find(CarouselItem);
    const lastItem = items.at(items.length - 1);

    expect(lastItem.props().isSelected).toEqual(true);
  });

  it('should select child ref on keypress of right chevron', () => {
    const wrap = mount(<ProductCarousel Images={Images} />);

    const divs = wrap.find('div');
    const rightChevron = divs.at(divs.length - 1);
    rightChevron.simulate('keypress', { key: 'Enter', keyCode: 13 });

    const items = wrap.find(CarouselItem);
    const lastItem = items.at(1);

    expect(lastItem.props().isSelected).toEqual(true);
  });
});
