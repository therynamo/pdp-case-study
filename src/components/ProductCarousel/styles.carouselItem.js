import { css } from 'emotion';

const styles = {
  img: css`
    width: 75px;
    height: 75px;
    margin: 0 2px;
  `,
  wrapper: css`
    outline: none;
    cursor: pointer;

    &:focus {
      border: 1px solid #999999;
      border-radius: 3px;
    }
  `,
  selected: css`
    border: 1px solid #999999;
    border-radius: 3px;
  `,
};

export default styles;
