import { css } from 'emotion';

const styles = {
  wrapper: css`
    width: 250px;
    overflow: scroll;
    display: flex;
  `,
};

export default styles;
