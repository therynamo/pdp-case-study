import { css } from 'emotion';

const styles = {
  carouselWrapper: css`
    display: flex;
    margin-top: 25px;
  `,
  chevron: css`
    margin: 0 10px;
    cursor: pointer;

    &::before {
      height: 0.45em;
      width: 0.45em;
      top: 37.5px;
      border-style: solid;
      border-width: 1px 1px 0 0;
      content: '';
      display: inline-block;
      position: relative;
      vertical-align: top;
    }
  `,
  back: css`
    transform: rotate(-135deg)
  `,
  forward: css`
    transform: rotate(45deg)
  `,
  productImage: css`
    width: 375px;
  `,
  wrapper: css`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    margin-bottom: 15px;
  `,
};

export default styles;
