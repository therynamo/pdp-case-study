import { css } from 'emotion';

// Icon styles from http://cssicon.space/#/icon/magnify

const styles = {
  magnify: css`
    color: #666;
    width: 12px;
    height: 12px;
    margin-right: 5px;
    border: solid 1px currentColor;
    border-radius: 100%;
    -webkit-transform: rotate(-45deg);
    transform: rotate(-45deg);

  &::before {
    content: '';
    position: absolute;
    top: 12px;
    left: 5px;
    height: 6px;
    width: 1px;
    background-color: currentColor;
  }

  & > i {
    position: absolute;
    left: 4px;
    top: 4px;
    -webkit-transform: rotate(45deg);
    transform: rotate(45deg);
  }

  & > i::before {
    content: '';
    position: absolute;
    width: 6px;
    height: 1px;
    background-color: currentColor;
  }

  & > i::after {
    content: '';
    position: absolute;
    width: 6px;
    height: 1px;
    background-color: currentColor;
    -webkit-transform: rotate(90deg);
    transform: rotate(90deg);
  }`,
  wrapper: css`
    display: flex;
    position: relative;
    align-items: center;
    padding-top: 15px;
    cursor: pointer;

    & > p {
      font-size: 13px;
      color: #666;
    }
  `,
};

export default styles;
