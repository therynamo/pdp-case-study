import React from 'react';
import PropTypes from 'prop-types';

import styles from './styles';

const ProductHighlights = ({ highlights }) => (
  <div className={styles.wrapper}>
    <h2 className={styles.h2}>product highlights</h2>
    <ul className={styles.list}>
    {
      highlights.features.map((highlight, i) => (
        <li key={i} dangerouslySetInnerHTML={{ __html: highlight }}/>
      ))
    }
    </ul>
  </div>
);

ProductHighlights.propTypes = {
  highlights: PropTypes.shape({
    features: PropTypes.array,
  }),
};

ProductHighlights.defaultProps = {
  highlights: { features: [] },
};

export default ProductHighlights;
