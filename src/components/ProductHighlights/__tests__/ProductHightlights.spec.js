import React from 'react';
import renderer from 'react-test-renderer'

import ProductHighlights from '../ProductHighlights';

describe('ProductHighlights', () => {
  it('should render without props', () => {
    const wrap = renderer.create(<ProductHighlights />);

    expect(wrap).toMatchSnapshot();
  });

  it('should render correctly with props', () => {
    const wrap = renderer.create(
      <ProductHighlights
        highlights={{ features: ["<strong>Wattage Output:</strong> 1100 Watts"]}}
      />
    );

    expect(wrap).toMatchSnapshot();
  })
});
