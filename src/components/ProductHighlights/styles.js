import { css } from 'emotion';

const styles = {
  h2: css`
    font-size: 23px;
    font-weight: bold;
  `,
  list: css`
    list-style-type: none;
    padding: 0;
    padding-left: 0px;
    margin-top: 15px;

    & > li {
      position: relative;
      font-size: 13px;
      margin-left: 10px;
      color: #666;

      @media(min-width: 420px) {
        padding: 2px 0;
      }
    }

    & > li::before {
      content: ".";
      position: absolute;
      top: -0.25em;
      left: -10px;

      @media(min-width: 420px) {
        top: -0.3em;
        padding: 2px 0;
      }
    }
  `,
  wrapper: css`
    padding: 0 5px 15px 5px;
  `,
};

export default styles;
