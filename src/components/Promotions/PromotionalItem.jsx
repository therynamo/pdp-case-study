import React from 'react';
import PropTypes from 'prop-types';

import Tag from './Tag';

import styles from './styles.promotionalItem';

const PromotionalItem = ({ promotion }) => {
  const description = promotion.Description[0] || "";
  const shortDescription = description.shortDescription;

  return (
    <div className={styles.wrapper}>
      <Tag />
      <span className={styles.description}>{shortDescription}</span>
    </div>
  );
};

PromotionalItem.propTypes = {
  promotion: PropTypes.shape({
    "Description": PropTypes.arrayOf(PropTypes.object),
    "endDate": PropTypes.string,
    "promotionIdentifier": PropTypes.string,
    "promotionType": PropTypes.string,
    "startDate": PropTypes.string,
  }),
};

PromotionalItem.defaultProps = {
  promotion: { Description: [] },
};

export default PromotionalItem;
