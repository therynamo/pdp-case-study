import React from 'react';
import PropTypes from 'prop-types';

import PromotionalItem from './PromotionalItem';

import styles from './styles';

const Promotions = ({ promotions }) =>(
  <div className={styles.wrapper}>
    {
      promotions.map((promotion, i) => (
        <PromotionalItem key={i} promotion={promotion} />
      ))
    }
  </div>
);

Promotions.propTypes = {
  promotions: PropTypes.arrayOf(PropTypes.object),
};

Promotions.defaultProps = {
  promotions: [],
};

export default Promotions;
