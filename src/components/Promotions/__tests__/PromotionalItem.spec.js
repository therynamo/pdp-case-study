import React from 'react';
import renderer from 'react-test-renderer';
import { shallow } from 'enzyme';

import PromotionalItem from '../PromotionalItem';

describe('PromotionalItem', () => {
  it('should render without props', () => {
    const wrap = renderer.create(<PromotionalItem />);

    expect(wrap).toMatchSnapshot();
  });

  it('should render proper promotion text', () => {
    const promotion = {
      Description: [{
        shortDescription: 'Luke, I am your father',
      }],
    }
    const wrap = shallow(<PromotionalItem promotion={promotion}/>);

    const span = wrap.find('span');

    expect(span.text()).toEqual('Luke, I am your father');
  });
});
