import React from 'react';
import renderer from 'react-test-renderer';
import { shallow } from 'enzyme';

import Promotions from '../Promotions';
import PromotionalItem from '../PromotionalItem'

describe('Promotions', () => {
  it('should render without props', () => {
    const wrap = renderer.create(<Promotions />);

    expect(wrap).toMatchSnapshot();
  });

  it('should create a list of Promotional Items', () => {
    const promotionalItems = [{ Description: [{}] }, { Description: [{}] }]
    const wrap = shallow(<Promotions promotions={promotionalItems} />);

    const items = wrap.find(PromotionalItem);

    expect(items.length).toEqual(2);
  });
});
