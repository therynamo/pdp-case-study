import { css } from 'emotion';

const styles = {
  wrapper: css`
    margin-left: 5px;
  `,
};

export default styles;
