import { css } from 'emotion';

const styles = {
  wrapper: css`
    display: flex;
    align-items: center;
  `,
  description: css`
    color: #C91300;
    font-size: 13px;
    padding-left: 5px;
  `,
};

export default styles;
