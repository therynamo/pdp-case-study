import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { css } from 'emotion';

import styles from './styles';

class QuantityPicker extends Component {
  constructor(props) {
    super(props);

    this.textInput = React.createRef();

    this.state = {
      input: 1,
    }
  }

  increment = () => {
    const { onClick } = this.props;
    const input = this.state.input;

    if (input > 98) return;

    this.setState(() => ({
      input: this.state.input + 1,
    }), () => onClick(this.state.input));
  }

  decrement = () => {
    const { onClick } = this.props;
    const input = this.state.input;

    if (input < 1) return;

    this.setState(() => ({
      input: input - 1,
    }), () => onClick(this.state.input));
  }

  handleInputOnClick = (e) => {
    e.preventDefault();

    this.textInput.current.select();
  };

  handleOnChange = (e) => {
    const input = e.target.value;
    let value;

    try {
      value = parseInt(input, 10);
    } catch(e) {
      value = 0;
    }
    this.setState(() => ({ input: value }));
  }

  render() {
    return (
      <div className={styles.wrapper}>
        <div>
          <span>quantity:</span>
        </div>
        <div className={styles.quantity}>
          <button
            aria-label="decrease quantity"
            className={css`${styles.button} ${styles.decrement}`}
            onClick={this.decrement}
          />
          <input
            type="number"
            ref={this.textInput}
            onClick={this.handleInputOnClick}
            onChange={this.handleOnChange}
            value={this.state.input}
          />
          <button
            aria-label="increase quantity"
            className={css`${styles.button} ${styles.increment}`}
            onClick={this.increment}
          />
        </div>
      </div>
    )
  }
}

QuantityPicker.propTypes = {
  onClick: PropTypes.func,
};

QuantityPicker.defaultProps = {
  onClick: () => {},
};

export default QuantityPicker;
