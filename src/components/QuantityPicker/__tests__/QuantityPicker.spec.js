import React from 'react';
import renderer from 'react-test-renderer';
import { shallow } from 'enzyme';

import QuantityPicker from '../QuantityPicker';

describe('QuantityPicker', () => {
  it('should render without props', () => {
    const wrap = renderer.create(<QuantityPicker />);
    expect(wrap).toMatchSnapshot();
  });

  it('should increment when ➕ is clicked', () => {
    const wrap = shallow(<QuantityPicker />);

    let input = wrap.find('input');

    expect(input.props().value).toEqual(1);

    const incrementButton = wrap.find('button').at(1);
    incrementButton.simulate('click');
    input = wrap.find('input');

    expect(input.props().value).toEqual(2);
  });

  it('should decrement when ➖ is clicked', () => {
    const wrap = shallow(<QuantityPicker />);

    let input = wrap.find('input');

    expect(input.props().value).toEqual(1);

    const decrementButton = wrap.find('button').at(0);
    decrementButton.simulate('click');
    input = wrap.find('input');

    expect(input.props().value).toEqual(0);
  });

  it('should call onClick when ➕ or ➖ is clicked', () => {
    const clickStub = jest.fn();
    const wrap = shallow(<QuantityPicker onClick={clickStub}/>);
    const incrementButton = wrap.find('button').at(1);
    const decrementButton = wrap.find('button').at(0);
    incrementButton.simulate('click');
    decrementButton.simulate('click');

    expect(clickStub).toHaveBeenCalledTimes(2);
  });
});
