import { css } from 'emotion';

const styles = {
  wrapper: css`
    width: 43%;
    display: flex;
    justify-content: space-between;
    margin-left: 5px;
    margin-top: 15px;
    margin-bottom: 15px;
    border: 1px solid #CCC;
    border-radius: 3px;
    padding: 3px 5px;

    & * span {
      font-size: 11px;
    }
  `,
  button: css`
    position: relative;
    width: 25px;
    height: 25px;
    background-color: #CCC;
    border: none;
    border-radius: 15px;
    cursor: pointer;

    &:before,
    &:after {
      content: "";
      position: absolute;
      background-color: white;
    }
  `,
  increment: css`
    &::before{
      top: 0;
      left: 50%;
      width: 1px;
      height: 70%;
      margin-left: -1px;
      margin-top: 4px;
    }

    &::after{
      top: 50%;
      left: 0;
      width: 70%;
      height: 1px;
      margin-left: 4px;
    }

    @media(min-width: 420px) {
      &::before {
        margin-left: 0;
        margin-top: 3px;
      }

      &::after {
        top: 49%;
      }
    }
  `,
  decrement: css`
    &:after{
      top: 50%;
      left: 0;
      width: 60%;
      height: 1px;
      margin-left: 5px;
    }
  `,
  quantity: css`
    display: flex;

    & > input {
      width: 15px;
      font-weight: bold;
      text-size: 16px;
      text-align: center;
      border: 0;
    }

    & > input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
          -webkit-appearance: none;
          margin: 0;
        }
  `,
};

export default styles;
