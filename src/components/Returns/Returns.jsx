import React from 'react';

import styles from './styles';

const Returns = () => (
  <div className={styles.wrapper}>
    <div className={styles.returns}>returns</div>
    <div className={styles.policy}>
      <span>This item must be returned within 30 days of the ship date. See <a>return policy</a> for details.</span>
      <span>Prices, promotions, styles and availability may vary by store and online.</span>
    </div>
  </div>
);

export default Returns;
