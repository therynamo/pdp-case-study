import React from 'react';
import renderer from 'react-test-renderer';

import Returns from '../Returns';

describe('Returns', () => {
  it('should render static text', () => {
    const wrap = renderer.create(<Returns />);
    expect(wrap).toMatchSnapshot();
  });
});
