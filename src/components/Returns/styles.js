import { css } from 'emotion';

const styles = {
  wrapper: css`
    display: flex;
    flex: 0 0 0;
    align-items: center;
    margin-left: 5px;
    margin-bottom: 15px;
  `,
  returns: css`
    font-size: 13px;
    border-right: 1px #CCC solid;
    color: #666;
    padding-right: 5px;
  `,
  policy: css`
    font-size: 9px;
    color: #666;
    margin-left: 5px;
  `,
};

export default styles;
