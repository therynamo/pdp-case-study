import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

const EmptyStar = ({ size }) => {
  let width="15px";
  let height="15px";

  switch (size) {
    case 'small':
      break;
    case 'medium':
      width= "20px"
      height="20px";
      break;
    case 'large':
      width= "30px"
      height="30px";
      break;
    default:
  };

  return (
    <Fragment>
        <svg id="icon-star-empty" viewBox="0 0 1024 1024" width={width} height={height}>
          <title>star-empty</title>
          <path className="path1" d="M1024 397.050l-353.78-51.408-158.22-320.582-158.216 320.582-353.784 51.408 256 249.538-60.432 352.352 316.432-166.358 316.432 166.358-60.434-352.352 256.002-249.538zM512 753.498l-223.462 117.48 42.676-248.83-180.786-176.222 249.84-36.304 111.732-226.396 111.736 226.396 249.836 36.304-180.788 176.222 42.678 248.83-223.462-117.48z"></path>
        </svg>
    </Fragment>
  );
};

EmptyStar.propTypes = {
  size: PropTypes.string,
};

EmptyStar.defaultProps = {
  size: 'small'
};

export default EmptyStar;
