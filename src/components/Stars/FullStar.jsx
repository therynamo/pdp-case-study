import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

import styles from './styles.fullStar';

const FullStar = ({ size }) => {
  let width = "15px";
  let height = "15px";

  switch (size) {
    case 'small':
      break;
    case 'medium':
      width= "20px"
      height="20px";
      break;
    case 'large':
      width= "30px"
      height="30px";
      break;
    default:
  };

  return (
    <Fragment>
      <svg id="icon-star-full" viewBox="0 0 1024 1024" width={width} height={height}>
        <title>star-full</title>
        <path className={styles.path} d="M1024 397.050l-353.78-51.408-158.22-320.582-158.216 320.582-353.784 51.408 256 249.538-60.432 352.352 316.432-166.358 316.432 166.358-60.434-352.352 256.002-249.538z"></path>
      </svg>
    </Fragment>
  );
};

FullStar.propTypes = {
  size: PropTypes.string,
};

FullStar.defaultProps = {
  size: 'small'
};

export default FullStar;
