import React from 'react';
import PropTypes from 'prop-types';
import Rating from 'react-rating';

import EmptyStar from './EmptyStar';
import FullStar from './FullStar';

import styles from './styles';

const Stars = ({ rating, readonly, size, text }) => (
  <div className={styles.wrapper}>
    <Rating
      initialRating={parseInt(rating, 10)}
      fractions={2}
      readonly={readonly}
      emptySymbol={<EmptyStar size={size} />}
      fullSymbol={<FullStar size={size} />}
    />
    {
      text && <span className={styles.text} >{text}</span>
    }
  </div>
);

Stars.propTypes = {
  rating: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string,
  ]),
  readonly: PropTypes.bool,
  size: PropTypes.string,
  text: PropTypes.string,
};

Stars.defaultProps = {
  rating: 0,
  readonly: true,
  size: 'small',
  text: "",
};

export default Stars;
