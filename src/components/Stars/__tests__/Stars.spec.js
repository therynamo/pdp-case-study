import React from 'react';
import renderer from 'react-test-renderer';
import { shallow } from 'enzyme';

import Stars from '../Stars';

describe('Stars', () => {
  it('should render without props', () => {
    const wrap = renderer.create(<Stars />);

    expect(wrap).toMatchSnapshot();
  });
  it('shoudl display text when passed', () => {
    const wrap = shallow(<Stars text="what is going on" />);

    const span = wrap.find('span');
    expect(span.text()).toEqual('what is going on');
  });
});
