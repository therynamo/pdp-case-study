import { css } from 'emotion';

const styles = {
  text: css`
    font-size: 13px;
    font-weight: bold;
    margin-left: 10px;
  `,
  wrapper: css`
    display: flex;
    align-items: center;
  `,
};

export default styles;
