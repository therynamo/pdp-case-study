import React from 'react';
import renderer from 'react-test-renderer';
import { shallow } from 'enzyme';

import ProductContainer from '../index';
import data from '../../../data/item-data.json';

describe('ProductContainer', () => {
  let productData = {};

  beforeEach(() => {
    productData = data.CatalogEntryView[0];
  });

  it('should render', () => {
    const wrap = renderer.create(<ProductContainer data={productData} />);

    expect(wrap).toMatchSnapshot();
  });

  it('should disable add to cart button', () => {
    productData.purchasingChannelCode = "9";

    const wrap = shallow(<ProductContainer data={productData}/>);
    const a2cButton = wrap.find('button').at(1);

    expect(a2cButton.props().disabled).toEqual(true);
  });

  it('should disable pick up in store button', () => {
    productData.purchasingChannelCode = "9";

    const wrap = shallow(<ProductContainer data={productData}/>)
    const puisButton = wrap.find('button').at(0);

    expect(puisButton.props().disabled).toEqual(true);
  });
});
