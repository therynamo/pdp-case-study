import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { css } from 'emotion';

import ProductCarousel from '../../components/ProductCarousel';
import Price from '../../components/Price';
import Promotions from '../../components/Promotions';
import QuantityPicker from '../../components/QuantityPicker';
import Returns from '../../components/Returns';
import ProductHighlights from '../../components/ProductHighlights';

import styles from './styles';

class ProductContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      items: 1,
    }
  }

  onQuantityChange = (value) => {
    this.setState(() => ({
      items: value,
    }))
  }

  render() {
    const {
      title,
      Offers = [],
      Promotions: promotions = [],
      ItemDescription = [],
      Images = [],
      purchasingChannelCode,
    } = this.props.data;
    const offer = Offers[0] || { OfferPrice: [] };
    const offerPrice  = offer.OfferPrice[0] || {};
    const highlights = ItemDescription[0] || {};
    const showA2C = purchasingChannelCode === "0" || purchasingChannelCode === "1";
    const showPUIS = purchasingChannelCode === "0" || purchasingChannelCode === "2";

    return (
      <div className={styles.productContainer}>
        <div className={styles.productPresentation}>
          <h1 className={styles.productTitle}>{title}</h1>
          <ProductCarousel Images={Images}/>
        </div>
        <div className={styles.productDetails}>
          <Price
            qualifier={offerPrice.priceQualifier}
            price={offerPrice.formattedPriceValue}
          />
          <hr className={styles.hr} />
          <Promotions promotions={promotions}/>
          <hr className={styles.hr} />
          <div className={styles.quantityWrapper}>
            <QuantityPicker onClick={this.onQuantityChange}/>
            <div className={styles.quantitySpacer} />
          </div>
          <div className={styles.callToAction}>
            <div className={styles.puisWrapper}>
              <button
                className={css`${styles.button} ${styles.puis}`}
                disabled={!showPUIS}
                onClick={() => alert(`You placed an order for ${this.state.items} item(s)`)}
              >
                pick up in store
              </button>
              <p className={styles.findInAStore}>find in a store</p>
            </div>
            <button
              className={css`${styles.button} ${styles.addToCart}`}
              disabled={!showA2C}
              onClick={() => alert(`${this.state.items} item(s) were added to your cart`)}
            >
              add to cart
            </button>
          </div>
          <Returns />
          <div className={styles.share}>
            <button>ADD TO REGISTRY</button>
            <button>ADD TO LIST</button>
            <button>SHARE</button>
          </div>
          <ProductHighlights highlights={highlights} />
        </div>
      </div>
    )
  }
}

ProductContainer.propTypes = {
  data: PropTypes.shape({
    Images: PropTypes.array,
    ItemDescription: PropTypes.array,
    Offers: PropTypes.array,
    Promotions: PropTypes.array,
    ReturnPolicy: PropTypes.array,
    buyable: PropTypes.string,
    eligibleFor: PropTypes.string,
    inventoryCode: PropTypes.string,
    inventoryStatus: PropTypes.string,
    purchasingChannelCode: PropTypes.string,
    title: PropTypes.string,
  }),
};

ProductContainer.defaultProps = {
  data: {},
};

export default ProductContainer;
