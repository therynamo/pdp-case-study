import { css } from 'emotion';

const styles = {
  addToCart: css`
    color: #FFF;
    background-color: #cc0000;

    &:disabled {
      background-color: #CCC;
    }
  `,
  button: css`
    width: 160px;
    height: 30px;
    border-radius: 3px;
    background-color: grey;
    cursor: pointer;

    @media(min-width: 420px) {
      width: 50%;
    }
  `,
  callToAction: css`
    display: flex;
    justify-content: center;
    margin: 0 5px 20px 5px;
  `,
  findInAStore: css`
    display: none;
    cursor: pointer;

    @media(min-width: 420px) {
      display: initial;
      font-size: 9px;
      font-weight: bold;
    }
  `,
  hr: css`
    color: #CCC;
  `,
  productContainer: css`
    display: flex;
    align-items: center;
    flex-direction: column;

    @media (min-width: 420px) {
      flex-direction: row;
    }
  `,
  productDetails: css`
    margin: 0 15px;
    max-width: 420px;
  `,
  productPresentation: css`
    display: flex;
    align-items: center;
    flex-direction: column;

    @media(min-width: 420px) {
      height: 100%;
    }
  `,
  productTitle: css`
    font-size: 20px;
    font-weight: lighter;
    text-align: center;
    width: 300px;

    @media(min-width: 420px) {
      font-size: 23px;
      width: 350px;
    }
  `,
  puis: css`
    color: #FFF;
    background-color: #000;
    margin-right: 17px;

    &:disabled {
      background-color: #CCC;
    }

    @media(min-width: 420px) {
      width: 95%;
    }
  `,
  puisWrapper: css`
    display: flex;
    flex-direction: column;
    width: 52%;
    align-items: center;
  `,
  quantitySpacer: css`
    width: 50%;
  `,
  quantityWrapper: css`
    display: flex;
    justify-content: space-between;

    @media(min-width: 420px) {

    }
  `,
  share: css`
    display: flex;
    justify-content: space-between;
    margin-bottom: 15px;

    & > button {
      width: 110px;
      height: 20px;
      font-size: 10px;
      font-weight: 100;
      background-color: #F3F3F3;
      border: none;
      border-radius: 3px;
      cursor: pointer;
    }

    @media(min-width: 420px) {
      & > button {
        width: 135px;
      }
    }
  `,
};

export default styles;
