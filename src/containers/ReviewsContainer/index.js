import React from 'react';
import PropTypes from 'prop-types';

import Stars from '../../components/Stars';
import ProConTable from '../../components/ProConTable';

import styles from './styles';

const ReviewsContainer = ({ data }) => {
  const { CustomerReview = [] } = data;
  const reviews = CustomerReview[0] || {};
  const pro = reviews.Pro[0] || {};
  const con = reviews.Con[0] || {};

  return (
    <div className={styles.wrapper}>
      <div className={styles.overall}>
        <Stars size="medium" rating={reviews.consolidatedOverallRating} text="overall" /> 
        <span className={styles.bold}> view all {reviews.totalReviews} reviews</span>
      </div>
      <div>
        <ProConTable
          pro={pro}
          con={con}
        />
      </div>
    </div>
  );
};

ReviewsContainer.propTypes = {
  data: PropTypes.shape({
    CustomerReview: PropTypes.arrayOf(PropTypes.object),
  }),
};

ReviewsContainer.defaultProps = {
  data: {}
};

export default ReviewsContainer;
