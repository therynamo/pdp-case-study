import { css } from 'emotion';

const styles = {
  bold: css`
    font-weight: bold;
    font-size: 13px;
  `,
  overall: css`
    width: 365px;
    display: flex;
    justify-content: space-between;
    align-items: center;
  `,
  wrapper: css`
    width: 370px;
    display: flex;
    flex-direction: column;
    justify-content: center;

    @media (min-width: 420px) {
    }
  `,
};

export default styles;
